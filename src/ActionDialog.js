/**
 * Created by pbarzev on 09/06/2017.
 */

import React, {Component} from 'react';
import {TextField, FlatButton, Dialog} from 'material-ui';

import './App.css';

class ActionDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newTitle: '',
            newDesc: '',
            newURL: '',
            errField1: '',
            errField2: '',
            errField3: '',
        }
    };

    /**
     * Handles the dialog submit
     */
    submit = (action) => {
        const {newTitle, newDesc, newURL} = this.state;
        const payload = action(newTitle, newDesc, newURL);
        console.log('payload', payload);
        const {errField1, errField2, errField3} = payload; // get data from payload

        // set the new error fields' state
        this.setState({errField1, errField2, errField3});

        if (payload.outcome) { // if dialog action has been successful, reset fields
            this.setState({newTitle: '', newDesc: '', newURL: ''}); // reset form state
        }
    };

    render() {
        const {dialogTitle, openDialog, action, handleClose} = this.props;
        const {errField1, errField2, errField3} = this.state;

        // dialog buttons
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                keyboardFocused={true}
                onTouchTap={() => this.submit(action)}
            />
        ];

        return (
            <Dialog
                title={dialogTitle}
                actions={actions}
                modal={false}
                open={openDialog}
                onRequestClose={this.handleClose}
            >
                <TextField
                    hintText="Title..."
                    fullWidth={true}
                    className="search"
                    errorText={errField1}
                    onChange={(ev, string) => this.setState({newTitle: string})}
                />
                <TextField
                    hintText="Description"
                    fullWidth={true}
                    className="searchBar"
                    errorText={errField2}
                    onChange={(ev, string) => this.setState({newDesc: string})}
                />
                <TextField
                    hintText="Image URL..."
                    fullWidth={true}
                    className="searchBar"
                    errorText={errField3}
                    onChange={(ev, string) => this.setState({newURL: string})}
                />

            </Dialog>
        );
    }

}

export default ActionDialog;


