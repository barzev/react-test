import React, { Component } from 'react';
import { AppBar, TextField, FloatingActionButton } from 'material-ui';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Grid, Col, Row } from 'react-bootstrap';
import axios from 'axios';
import ItemCards from './ItemCards';
import ActionDialog from './ActionDialog';
import validateForm from './lib/validateForm';

import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            receivedData: false,
            cakeData: [],
            query: '',
            newCakeDialog: false,
        }
    };

    /**
     * React lifecycle method.
     * Fires as soon as component has mounted.
     */
    componentDidMount() {
        // get cake data
        axios.get('https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json')
            .then((response) => {
                const cakeData = response.data;
                console.log('Received Data:', response.data); // log response
                // save to state
                this.setState({
                    receivedData: true,
                    cakeData
                });
            })
            .catch(function (error) {
                // output to console in case of error
                console.log(`error in loading external json data: ${error}`);
            });
    }

    /**
     * Saves the supplied query to the state.
     * @param query the user-supplied query
     */
    saveQuery = (query) => this.setState({ query });


    /**
     * Search cakes given the state query.
     * @param cakes the cakes to be searched
     * @returns {Array} the cakes that satisfy the search
     */
    searchCakes = (cakes) => {
        let { query } = this.state;
        // query in lowercase
        query = query.toLocaleLowerCase();
        // filter cakes
        return cakes.filter((cake) => (cake.title.toLowerCase().includes(query) || cake.desc.toLowerCase().includes(query)));
    };


    /**
     * Validates the dialog form. Creates a new cake item if valid data has been supplied.
     */
    createItem = (newTitle, newDesc, newURL) => {
        const payload = validateForm(newTitle, newDesc, newURL);

        if (payload.outcome) {
            const { cakeData } = this.state;
            // push to cake data array
            const newCake = [{ title: newTitle, desc: newDesc, image: newURL }];
            this.setState({ cakeData: newCake.concat(cakeData), newCakeDialog: false });
        }
        return payload;
    };

    /**
     * Closes new item dialog
     */
    handleClose = () => this.setState({ newCakeDialog: false });


    render() {
        const { newCakeDialog } = this.state;
        let cakes, dispCakes;
        const { receivedData, cakeData } = this.state;
        receivedData ? cakes = cakeData : cakes = null; // if data received get the cakes
        cakes ? dispCakes = this.searchCakes(cakes) : dispCakes = null; // final cakes to be displayed

        return (
            <div className="App">
                <AppBar
                    title="React Cakes"
                    iconStyleLeft={{ display: 'none' }}
                />
                <div className="wrapper">
                    <div className="content">
                        <Grid>
                            <Row className="clearSpace">
                                <Col xs={12}>
                                    <TextField
                                        hintText="Search cakes..."
                                        fullWidth={true}
                                        className="searchBar"
                                        onChange={(ev, query) => this.saveQuery(query)}
                                    />
                                </Col>
                            </Row>
                            <Row className="clearSpace">
                                <Col xs={2} xsOffset={10} md={1} mdOffset={11}>
                                    <FloatingActionButton
                                        mini={true}
                                        onTouchTap={() => this.setState({ newCakeDialog: true })}
                                    >
                                        <ContentAdd />
                                    </FloatingActionButton>
                                </Col>
                            </Row>
                            <Row className="clearSpace">
                                <Col xs={12}>
                                    <ItemCards itemData={dispCakes} />
                                </Col>
                            </Row>
                        </Grid>
                    </div>
                </div>

                <ActionDialog title={'Add a new cake'} handleClose={this.handleClose} action={this.createItem}
                    openDialog={newCakeDialog} />
            </div>
        );
    }
}

export default App;