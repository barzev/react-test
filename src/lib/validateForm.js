/**
 * Created by pbarzev on 09/06/2017.
 */

/**
 * Validates the form
 * @param newTitle title text field
 * @param newDesc description text field
 * @param newURL image URL text field
 * @returns {{outcome: boolean, errField1: string, errField2: string, errField3: string}} result and
 * any error fields
 */
const validateForm = (newTitle, newDesc, newURL) => {
    const REQ_FIELD = 'Required Field';
    const INVALID_IMG = 'The image URL needs to end in .jpg, .png or .gif'

    let payload = {    // initial dialog payload
        outcome: false,
        errField1: '',
        errField2: '',
        errField3: '',
    };

    // validate form data
    if (newTitle.trim() === "") payload.errField1 = REQ_FIELD;
    else if (newDesc.trim() === "") payload.errField2 = REQ_FIELD;
    else if (newURL.trim() === "") payload.errField3 = REQ_FIELD;
    else if (!(newURL.endsWith('.jpg') || newURL.endsWith('.png') || newURL.endsWith('.gif'))) payload.errField3 = INVALID_IMG;
    else payload.outcome = true;

    return payload;
};

export default validateForm;