/**
 * Created by pbarzev on 07/06/2017.
 */

import React, {Component} from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {Col} from 'react-bootstrap';
import FlatButton from 'material-ui/FlatButton';
import ActionDialog from './ActionDialog';
import validateForm from './lib/validateForm';

class ItemCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selCard: null,
            editCakeDialog: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        const {itemData} = nextProps;
        this.setState({data: itemData}); // save newly supplied props to state
    }

    /**
     * Edits a card.
     * @param newTitle title text field
     * @param newDesc description text field
     * @param newURL image URL text field
     */
    editItem = (newTitle, newDesc, newURL) => {
        const payload = validateForm(newTitle, newDesc, newURL);
        if (payload.outcome) {
            const {selCard, data} = this.state;
            // the edited cake
            const cake = {title: newTitle, desc: newDesc, image: newURL};
            // create a copy of the item data
            const newData = data.slice();
            newData[selCard] = cake;
            this.setState({data: newData, editCakeDialog: false});
        }
        return payload;
    };

    /**
     * Capitalises each word of a string.
     * @param str the string to be capitalised
     * @returns {string} the capitalised string
     */
    capWords(str) {
        return str.toLowerCase().replace(/\b\w/g, (x) => {
            return x.toUpperCase();
        });
    }

    /**
     * Creates a list of item cards.
     * @param itemData the item data received from the JSON source
     */
    createCards = (itemData) => {
        return itemData.map((cake, index) => {
            const title = cake.title;
            const image = cake.image;
            const desc = cake.desc;
            return (
                <Col xs={12} md={6} key={itemData.indexOf(cake)}>
                    <Card className="card">
                        <CardMedia className="cardTop">
                            <img className="itemImg" src={image} alt=""/>
                        </CardMedia>
                        <div className="cardBot">
                            <CardTitle title={this.capWords(title)}/>
                            <CardText>{desc}</CardText>
                            <CardActions>
                                <FlatButton
                                    label="Edit"
                                    onTouchTap={() => this.setState({editCakeDialog: true, selCard: index})}
                                />
                            </CardActions>
                        </div>
                    </Card>
                </Col>
            );
        });
    };

    /**
     * Closes new item dialog
     */
    handleClose = () => this.setState({editCakeDialog: false});

    render() {
        let cards;
        const {editCakeDialog} = this.state;
        const {data} = this.state;

        // check if any data has been supplied at all
        if (data !== null && data.length > 0) cards = this.createCards(data);

        return <div>
            {cards}
            <ActionDialog title={'Edit cake'} handleClose={this.handleClose} action={this.editItem}
                          openDialog={editCakeDialog}/>
        </div>
    }
}

export default ItemCards;