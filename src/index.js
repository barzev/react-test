/* global document */

/**
 * index.js
 * Root of the SPA.
 * Declares and initialises React Virtual DOM.
 * @author Petar Barzev
 * Last updated: 07/06/2017
 */

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

if (typeof document !== 'undefined') {
    ReactDOM.render(
        <MuiThemeProvider>
            <App />
        </MuiThemeProvider>
        ,
        document.getElementById('root'));
}
