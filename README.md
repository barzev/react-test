## COMPLETED

Using [create-react-app](https://facebook.github.io/react/blog/2016/07/22/create-apps-with-no-configuration.html), any other tool or library you wish, and 2-3 hours, please deliver the following:

- Render list of cakes (https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json)
- Make UI render nicely on iPad and Nexus 5
- Add ability to search cakes
- Add basic ability to edit/add cakes

Total dev time: 7-8 hours (broken up in a few days)
Iterative development and testing methodology. Exhaustive tests implemented.


![app screenshot.png](https://bitbucket.org/repo/5qdnqAb/images/4101288574-app%20screenshot.png)